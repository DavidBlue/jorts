# Jorts Device Report - 02392022-130734
## Hardware
#### Model
iPhone13,4
#### RAM
5.0 GB
#### Processors
6 of 6 active
#### Thermal State
Fair
#### Biometrics
FaceID
#### Battery Level
100%
#### Battery Status
Charging (Not in low power mode)
#### Orientation
Portrait
#### Is In Motion
Not In Motion

---
## Screen

#### Diagonal Size
-1.0 in
#### Resolution
428x926
#### Pixel Density
Couldn't determine pixels per inch
#### Ratio
-1.0:-1.0

---
## Software
#### Name
Jorts
#### Hostname
jorts.local
#### Operating System
Version 15.4 (Build 19E5219e)
#### Uptime
1 hours
#### Volume
0.8125
#### Preferred Language
en-US
#### Primary Locale
en
#### System Style
Light mode

---

## Storage
^^ Total
255.88 GB
^^ Used
158.33 GB (62%)
^^ Remaining
97.55 GB (38%)
