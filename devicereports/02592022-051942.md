# Jorts Device Report - 02592022-051939
## Hardware
#### Model
iPhone13,4
#### RAM
5.0 GB
#### Processors
6 of 6 active
#### Thermal State
Nominal
#### Biometrics
FaceID
#### Battery Level
91%
#### Battery Status
Charging (Not in low power mode)
#### Orientation
Portrait
#### Is In Motion
Not In Motion

---
## Screen

#### Diagonal Size
-1.0 in
#### Resolution
428x926
#### Pixel Density
Couldn't determine pixels per inch
#### Ratio
-1.0:-1.0

---
## Software
#### Name
Jorts
#### Hostname
jorts.local
#### Operating System
Version 15.4 (Build 19E5235a)
#### Uptime
2 hours
#### Volume
0.5
#### Preferred Language
en-US
#### Primary Locale
en
#### System Style
Dark mode

---

## Storage
^^ Total
255.88 GB
^^ Used
174.83 GB (68%)
^^ Remaining
81.05 GB (32%)
